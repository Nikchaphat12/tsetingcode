import math
import os
import random
import re
import sys

#
# Complete the 'alternate' function below.
#
# The function is expected to return an INTEGER.
# The function accepts STRING s as parameter.
#

def alternate(s):
    # Write your code here
     pl = [ii for ii in set(map(str, s))]
     m = 0
     for i in range(len(pl)-1):
         for j in range(i+1, len(pl)):
             b = None
             for ii in s:
                 if ii == pl[i]:
                    if b:
                        break
                    b = True
                 if ii == pl[j]:
                    if not b:
                        break
                    b = False
             else:
                 if m < s.count(pl[i])+s.count(pl[j]):
                    m = s.count(pl[i])+s.count(pl[j])
     return m
if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    l = int(input().strip())

    s = input()

    result = alternate(s)

    fptr.write(str(result) + '\n')

    fptr.close()
