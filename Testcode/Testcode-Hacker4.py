from hackerrank.StrFunny import StrFunny
import unittest

class FunnyStringTest(unittest.TestCase):
    
    def test_funnystring(self):
        string = 'acxz'
        isfunny = StrFunny(string)
        self.assertEqual('Funny',isfunny)
    
    def test_notfunnystring(self):
        string = 'bcxz'
        isfunny = StrFunny(string)
        self.assertEqual('Not Funny',isfunny)
    

