
from hackerrank.AlternatingCharacters import alternatingCharacters
import unittest

class AlternatingCharactersTest(unittest.TestCase):
    
    def test_alternatingdetect_1(self):
        string = 'AAABBBAABB'
        alternatingtest = alternatingCharacters(string)
        self.assertEqual(6,alternatingtest)
    
    def test_alternatingdetect_2(self):
        string = 'BBBBB'
        alternatingtest = alternatingCharacters(string)
        self.assertEqual(4,alternatingtest)
    
    def test_alternatingdetect_3(self):
        string = 'ABABABAB'
        alternatingtest = alternatingCharacters(string)
        self.assertEqual(0,alternatingtest)

