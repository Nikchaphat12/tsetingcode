from hackerrank.caesar import caesarCipher
import unittest


class CaesarCipherTest(unittest.TestCase):
    
    def test_caesar_shift_one(self):
        string = 'middle-Outz'
        shift = 2
        caesarshift = caesarCipher(string,shift)
        self.assertEqual('okffng-Qwvb',caesarshift)
    
    def test_caesar_shift_two(self):
        string = 'Always-Look-on-the-Bright-Side-of-Life'
        shift = 5
        caesarshift = caesarCipher(string,shift)
        self.assertEqual('Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj',caesarshift)
