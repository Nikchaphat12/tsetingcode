
from hackerrank.gridChallenge import gridChallenge
import unittest


class GridChallengeTest(unittest.TestCase):
    
    def test_gridchallenge_1(self):
        gridlist = ['ebacd', 'fghij', 'olmkn', 'trpqs', 'xywuv']
        gridtest = gridChallenge(gridlist)
        self.assertEqual('YES',gridtest)
    
    
    def test_gridchallenge_2(self):
        gridlist = ['mpxz', 'abcd', 'wlmf']
        gridtest = gridChallenge(gridlist)
        self.assertEqual('NO',gridtest)
